<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit2e3c80f8c95d145fce936dc0f241bc72
{
    public static $files = array (
        '7850bb9504139d3ff96ccbcdcd5f07e6' => __DIR__ . '/../..' . '/app/config.php',
        '9103ce8aaa724387df81acd57afeff50' => __DIR__ . '/../..' . '/app/routes.php',
    );

    public static $prefixLengthsPsr4 = array (
        'F' => 
        array (
            'Framework\\' => 10,
        ),
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Framework\\' => 
        array (
            0 => __DIR__ . '/../..' . '/framework',
        ),
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/app',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit2e3c80f8c95d145fce936dc0f241bc72::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit2e3c80f8c95d145fce936dc0f241bc72::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
